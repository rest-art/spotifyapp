var express = require('express');
var app = express();

app.use(express.static(__dirname + '/'));

app.listen(3001, 'localhost', function(){
    console.log(`Server running on http://localhost:3001/`);
});