# SpotifyApp
## Task#1
Javascript: Music search 

App allows the user to search for an artist or album using typeahead searching.

- Use a single combined field for searching both entities
- Click on 'show more' button under the artists list allows to load 20 more artists to the list
- For artists listed in the results click to display the albums from that artist
- After the artists displayed relevant albums and its pagination
- Click on the Spotify logo clean the results
- Click on the green icon open the artist / album in Spotify
- Queries are cached
- All the state stored in the local storage, so you can refresh the page and the results will not be lost

##DEMO http://mdiamond.ru/spotify/

### Local demo
- npm instal
- bower install
- gulp server

Then open http://localhost:3001/
