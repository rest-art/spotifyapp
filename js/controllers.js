'use strict';
angular.module('spotifyApp')

    .controller('IndexController', ['$scope', '$http', function ($scope, $http) {
        $scope.isStart = true;
        $scope.selectedArtist = '';
        $scope.key = '';
        $scope.pages = [];
        $scope.albums = [];
        $scope.artists = [];
        $scope.limitArtists = 5;
        $scope.pagination = {
            page: 0,
            total: 0,
            limit: 10,
            next: false,
        };
        const pag = $scope.pagination;

        $scope.preload = function () {
            if (localStorage['results']) {
                let results = JSON.parse(localStorage['results']);
                if (results.key) {
                    $scope.isStart = false;
                }
                _.merge($scope.pagination, results.pagination);
                $scope.selectedArtist = results.selected;
                $scope.limitArtists = results.limitArtists;
                $scope.artists = results.artists;
                $scope.albums = results.albums;
                $scope.key = results.key;
            }

            $scope.paginate();
        }

        $scope.search = function () {

            if ($scope.key == '') {
                $scope.clean();
                $scope.paginate();
            }
            else if (localStorage['results'] && $scope.key != JSON.parse(localStorage['results']).key) {
                delete $scope.selectedArtist;
                $scope.limitArtists = 5;
                pag.page = 0;
                update();
            } else {
                update();
            }

            function update() {
                $scope.isStart = false;

                let limit = pag.limit;
                let offset = (pag.page * limit);
                let url = 'https://api.spotify.com/v1/';
                let type = $scope.limitArtists ? 'artist,album' : 'album';

                if ($scope.selectedArtist) {
                    url += 'artists/'+$scope.selectedArtist.id+'/albums?offset='+offset+'&limit='+limit;
                } else {
                    url += 'search?type='+type+'&query='+$scope.key+'&offset='+offset+'&limit='+limit;
                }

                $http
                    .get(url, {cache: true})
                    .then(function (res) {
                        if (res.data.artists) {
                            $scope.artists = res.data.artists;
                        }
                        $scope.albums = $scope.selectedArtist ? res.data : res.data.albums;
                        $scope.paginate();
                        $scope.save();
                    });
            }
        }

        $scope.reset = function () {
            $scope.key = '';
            $scope.isStart = true;
            $scope.clean();
            document.getElementsByClassName("form-control")[0].focus();
        }

        $scope.clean = function () {
            pag.page = 0;
            $scope.albums = [];
            $scope.artists = [];
            delete $scope.selectedArtist;
            $scope.limitArtists = 5;
            $scope.save();
        }

        $scope.paginate = function () {
            pag.total = $scope.albums.total;
            pag.next = pag.total - (pag.page+1) * pag.limit > 0;

            let pageLength = Math.ceil(pag.total / pag.limit);
            let max = Math.ceil(pag.total - (pag.page+1) * pag.limit);

            if (pageLength <= 10) {
                $scope.pages = _.range(pageLength);
            } else if (pag.page > 5) {
                let from = pag.page - 5;
                let to = pag.page + 5;

                if (to > pageLength) {
                    from = pageLength-10;
                    to = pageLength;
                }
                $scope.pages = _.range(from, to);
            } else {
                $scope.pages = _.range(10);
            }
        }

        $scope.save = function () {
            localStorage['results'] = JSON.stringify({
                artists: $scope.artists,
                albums: $scope.albums,
                selected: $scope.selectedArtist,
                pagination: $scope.pagination,
                key: $scope.key,
                limitArtists: $scope.limitArtists,
            });
        }

        $scope.selectArtist = function (id) {
            $scope.selectedArtist = _.find($scope.artists.items, {id: id});
            $scope.save();
            $scope.artists = [];
            $scope.search();
        }

        $scope.removeSelected = function () {
            $scope.artists = JSON.parse(localStorage['results']).artists;
            delete $scope.selectedArtist;
            $scope.limitArtists = 5;
            $scope.pagination.page = 0;
            $scope.search();
        }

        $scope.more = function () {
            $scope.limitArtists = false;
            let url = 'https://api.spotify.com/v1/search?type=artist&query='+$scope.key+'&offset='+$scope.artists.items.length+'&limit=20';

            $http
                .get(url, {cache: true})
                .then(function (res) {
                    $scope.artists.items = $scope.artists.items.concat(res.data.artists.items);
                    $scope.save();
                });
        }

        $scope.go = function (num) {
            $scope.pagination.page = $scope.pagination.page + num;
            $scope.search();
        }

        $scope.openUrl = function (url) {
            window.open(url, '_blank');
        };

    }])
;