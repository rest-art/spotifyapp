'use strict';

angular.module('spotifyApp', ['ui.router','ngResource'])
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app', {
                url:'/',
                views: {
                    'search': {
                        templateUrl : 'view/search.html',
                        controller  : 'IndexController'
                    }
                }
            })
            .state('app.index', {
                url: ':key'
            });

        $urlRouterProvider.otherwise('/');
    })