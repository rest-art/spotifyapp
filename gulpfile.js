var gulp       = require('gulp');
var less       = require('gulp-less');
var rename     = require('gulp-rename');
var runseq     = require('run-sequence');
var minifycss  = require('gulp-minify-css');
var exec       = require('child_process').exec;

var dirs = {
    less: './less',
};




//
// ####  ##### #####   #   #   # #     #####
// #   # #     #      # #  #   # #       #
// #   # ####  ####  #   # #   # #       #
// #   # #     #     ##### #   # #       #
// #   # #     #     #   # #   # #       #
// ####  ##### #     #   #  ###  #####   #

gulp.task('default', function (cb) {
    runseq(
        ['less', 'watch-less', 'server'],
    cb);
});

gulp.task('less', function (cb) {
    runseq(
        ['less'],
    cb);
});

gulp.task('watch-less', function (cb) {
    runseq(
        ['watch-less'],
    cb);
});

gulp.task('less', function (cb) {
    gulp.src(dirs.less+'/**/*.less')
        .pipe(less())
        .pipe(minifycss())
        .pipe(rename("min.css"))
        .on('error', function(err) {
            console.error(err);
        })
        .pipe(gulp.dest(dirs.less));

    cb();
});

gulp.task('watch-less', function() {  
    gulp.watch('./less/**/*.less' , ['less']);
});

gulp.task('server', function (cb) {
    exec('node server.js', {maxBuffer : 1024 * 1024 * 1024}, function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});
